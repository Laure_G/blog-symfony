-- Active: 1673948009961@@127.0.0.1@3306@blog

DROP TABLE IF EXISTS article_category;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS article;

CREATE TABLE article (
        id INT PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        mainPicture VARCHAR(1000) NOT NULL,
        content VARCHAR(10000) NOT NULL,
        author VARCHAR(255) NOT NULL,
        date DATETIME,
        views INT
    );

CREATE TABLE category (
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL
    );

CREATE TABLE comment (
        id INT PRIMARY KEY AUTO_INCREMENT,
        author VARCHAR(255) NOT NULL,
        date DATETIME,
        content VARCHAR(1000) NOT NULL,
        id_article INT,
        FOREIGN KEY (id_article) REFERENCES article(id) ON DELETE SET NULL
    );

CREATE TABLE image (
        id INT PRIMARY KEY AUTO_INCREMENT,
        link VARCHAR(1000) NOT NULL,
        id_article INT,
        FOREIGN KEY (id_article) REFERENCES article(id) ON DELETE SET NULL
    );

CREATE TABLE article_category (
    id_article INT,
    id_category INT,
    PRIMARY KEY (id_article,id_category),
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE,
    Foreign Key (id_category) REFERENCES category(id) ON DELETE CASCADE
);

INSERT INTO article (title, mainPicture, content, author, date, views) VALUES ("L'escalier d'Oscar Niemeyer", "https://media.admagazine.fr/photos/60c762ea7f6f5c74ef8d09f1/master/w_2580%2Cc_limit/stair_itamaraty_niemeyer_brasi__lia_jpg_2535.jpg", "L'escalier d'Oscar Niemeyer au palais Itamaraty à BrasiliaCe bâtiment de béton conçu en 1970 par Oscar Niemeyer héberge le ministère des Affaires étrangères du Brésil. D'une beauté sculpturale, l'escalier semble presque flotter en l'absence de garde-corps et de contre-marches, ses marches très fines se déployant en éventail dans une courbe tout en fluidité.
","Blandine Frisina", "2023-01-12",1),("L'escalier du jardin Las Pozas", "https://media.admagazine.fr/photos/60c762ea0ce773818d8d245b/master/w_2580%2Cc_limit/capture_d___e__cran_2017_05_18_a___14_47_33_png_1653.png", "L'escalier du jardin Las Pozas au Mexique. En plein milieu de la jungle mexicaine, à Xilitla, le poète britannique Edward James a réalisé, entre 1962 et 1984, un jardin surréaliste de 32 hectares ponctué d'une trentaine de folies. Les sculptures en béton se fondent dans la végétation environnante au milieu des cascades naturelles et des étangs. En plein cœur de cet éden, l'Escalier vers le paradis apparaît dans ce décor comme une vision fantasmagorique.","Juiletta Simon", "2023-01-24 17:00",3),
("Le Chand Baori","https://media.admagazine.fr/photos/60c762ea8f79e7f55b0f2d46/master/w_2580%2Cc_limit/escalier_inde_jpg_5043.jpg","Le Chand Baori en IndeCreusé au VIIIe siècle à la demande du roi Chanda, ce vaste puits à degrés situé dans l’état du Rajasthan a été bâti sur un plan carré et une profondeur de 30 mètres, de façon à recueillir l’eau de pluie pendant la mousson. Ses 3500 marches réparties sur treize étages en font un lieu surprenant à l’esthétique cinégénique. Pas étonnant que Christopher Nolan ait choisi cet endroit pour en faire la prison du héros dans The Dark Knight Rises, dernier volet de la trilogie Batman.","Edward Lone", '2023-01-24 17:00',2 ), 
("L'escalier de Bramante","https://media.admagazine.fr/photos/60c762ea06d5708eaeafab05/master/w_2580%2Cc_limit/escalier_bramante_jpg_94.jpg","L'escalier de Bramante à RomeComme son nom ne l’indique pas, ce majestueux escalier à double hélice qui sert de rampe d'accès aux musées du Vatican n’est pas l’œuvre de Donato d’Angelo dit Bramante, mais de l’architecte Giuseppe Momo en 1932. Bramante a bien réalisé un double escalier en spirale semblable à celui-ci en 1512 qui se trouve encore aujourd’hui au Vatican, dans la cour du Belvédère.","J-F Champollion", '2023-01-22 15:00',4),
("L'escalier de la boutique Olivetti", "https://media.admagazine.fr/photos/60c762ea9f11a1f651e4220f/master/w_2580%2Cc_limit/2_olivetti_carlo_scarpa_jpg_5572_jpeg_8636_jpeg_north_700x_white_jpg_6412.jpg", "L'escalier de la boutique Olivetti à Venise.C'est en 1957 que l’entreprise confie l’architecture de sa boutique place Saint-Marc à l'architecte vénitien Carlo Scarpa. Restaurée en 2011 grâce au FAI (fonds pour l'environnement italien) et transformée en musée, elle ouvre au public qui découvre alors ce joyau architectural. Élément central de l'espace, l’escalier en pierre d’Istrie rappelle, avec ses marches asymétriques, les retours chariot des machines à écrire.","James Meziou", "2023-02-01",3),
("L'escalier de la tour Alberaria", "https://media.admagazine.fr/photos/60c762ea7e036d99ddc2cb71/master/w_2580%2Cc_limit/capture_d___e__cran_2017_05_18_a___15_19_23_png_2795.png", "L'escalier de la tour Alberaria à Venise, connue également sous le nom de Torre di Porta Nuova, cette tour de 35 mètres de hauteur datant du début du XIXe siècle, située au sein de l'arsenal de Venise, abrite un escalier doté d'un impressionnant mécanisme de roue maintenue entre deux murs épais. Restructurée par le studio d'architectes MAP Studio, la tour a réouvert au public en 2011 et fait désormais partie du parcours de la Biennale.","Nicolas Dupont", "2023-01-24",3),
("L'escalier du château de Chambord", "https://media.admagazine.fr/photos/60c762ea0ce773818d8d245d/master/w_2580%2Cc_limit/sl_5245____dnc_photo_sophie_lloyd_jpg_8916.jpg", "L'escalier du château de Chambord. L’escalier à doubles révolutions est sans nul doute l’élément architectural le plus marquant du château, et sa fascination depuis le XVIe siècle ne s’est jamais démentie. Sa place au centre de l’immense donjon carré le met en effet en scène d’une manière inédite : autour de son noyau central, deux rampes d’escalier s’enroulent l’une au-dessus de l’autre afin de desservir les étages principaux, tout en permettant à deux personnes l'empruntant de ne jamais se rencontrer. Une merveille d’inventivité influencée par le travail de Léonard de Vinci.","Marie Sevy", "2023-01-24 17:00",3),("L'escalier du Jardin de la spéculation cosmique", "https://media.admagazine.fr/photos/60c762ea54ae0c6b70af2a0f/master/w_2580%2Cc_limit/cosmic_garden_jpg_3562.jpg", "L'escalier du Jardin de la spéculation cosmique en Écosse. Cette impressionnante volée de marches blanches zigzaguant à travers la verdure se trouve dans le Jardin de la spéculation cosmique, près de Glasgow. Très éloigné du jardin à l’anglaise tel qu’on se le représente, cet endroit étonnant de 15 hectares accueille, en lieu et place des traditionnels bosquets et autres parterres, des sculptures inspirées de la théorie mathématique du chaos et de la géométrie fractale. Tout un programme… initié par l’architecte paysagiste et théoricien Charles Jencks.","John Doe", "2022-12-22",2),("L'escalier du Palais d'Iéna", "https://media.admagazine.fr/photos/60c762ea06d5708eaeafab07/master/w_2580%2Cc_limit/l___art_prend_son_envol_au_palais_d___i__na_9435_jpeg_north_1160x_white_jpg_1840.jpg", "L'escalier du Palais d'Iéna à Paris. À partir de la salle des pas perdus, on accède aux niveaux supérieur et inférieur du Palais d'Iéna par un escalier monumental en béton à double révolution pensé par l'architecte Auguste Perret. Pièce maîtresse de l'édifice, cet escalier est une véritable prouesse car si, à première vue il semble ne reposer sur rien et paraît presque s'envoler en une arabesque aérienne, son coffrage a nécessité de contourner de nombreuses difficultés techniques.","Déborah Martinez", "2023-01-24",5),("L'escalier de la Quinta da Regaleira", "https://media.admagazine.fr/photos/60c762ea9f11a1f651e42210/master/w_2580%2Cc_limit/regaleira_jpg_4319.jpg", "L'escalier de la Quinta da Regaleira près de LisbonneÀ Sintra, près de Lisbonne, la Quinta da Regaleira est un palais construit au début du XXe siècle par le millionnaire António Augusto Carvalho Monteiro. Il abrite le Puits initiatique, une tour inversée de 27 mètres dotée d'un escalier en spirale censé mener des ténèbres vers la lumière en passant par neuf paliers, métaphore des neuf cercles de la Divine Comédie de Dante.","Alexandre Legrand", "2023-01-24 17:00",2), ("L'escalier du Peñón de Guatapé ", "https://media.admagazine.fr/photos/60c762eab3898c083e89a70c/master/w_2580%2Cc_limit/colombie_jpg_7486_jpeg_8452.jpeg", "L'escalier du Peñón de Guatapé en Colombie. Situé près de Medellin, le Peñón de Guatapé est un monolithe de 220 mètres de haut qui contient plusieurs fissures, dont une où un escalier de 740 marches zigzague jusqu'au sommet offrant un panorama sur toute la région d'Antioquia.","Blandine Frisina", "2023-01-12",1);

INSERT INTO category (name) VALUES ("Architecture"), ("Site archéologique"),("Site historique");

INSERT INTO comment (author, date,content,id_article) VALUES("Max","2023-01-24 20:00","Incroyable lieux à ne pas loupé si vous passez au Mexique",1),("Jeanne","023-01-27 15:00'","Un des joyeux du Tibet",2) ;

INSERT INTO image (link, id_article) VALUES  ("https://m1.quebecormedia.com/emp/emp/CC8c6b181e4-4898-4d7c-aad1-3da0221248fe_ORIGINAL.jpg?impolicy=crop-resize&x=0&y=0&w=780&h=439&width=1300", 1), ("https://m1.quebecormedia.com/emp/emp/CC950ed76af-1f6b-4d10-b1d1-93fcee0c4457_ORIGINAL.jpg?impolicy=crop-resize&x=0&y=0&w=780&h=439&width=1300",2);


INSERT INTO article_category (id_article, id_category) VALUES (1,1),(1,2), (2,3), (2,1);