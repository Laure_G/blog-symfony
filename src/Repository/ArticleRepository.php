<?php

namespace App\Repository;

use App\Entities\Article;
use DateTime;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class ArticleRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM article');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $articles[] = $this->sqlToArticle($item);
        }
        return $articles;
    }

    public function findById(int $id):?Article {
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToArticle($result);
        }
        return null;
    }

    private function sqlToArticle(array $line):Article {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Article($line['title'], $line['mainPicture'], $line['content'], $line['author'],$date,$line['views'], $line['id']);
    }

    public function persist(Article $article) {
        $statement = $this->connection->prepare('INSERT INTO article (title,mainPicture,content,author,date,views) VALUES (:title,:mainPicture, :content, :author, :date,:views)');
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('mainPicture', $article->getMainPicture());
        $statement->bindValue('content', $article->getContent());
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('views', $article->getViews());


        $statement->execute();

        $article->setId($this->connection->lastInsertId());

    }


    public function update(Article $article)
    {
        $statement = $this->connection->prepare("UPDATE article SET title = :title, mainPicture = :mainPicture, content=:content, author=:author, date=:date,views=:views WHERE id=:id");
        $statement->bindValue(":id", $article->getId(), PDO::PARAM_INT);
        $statement->bindValue(":title", $article->getTitle());
        $statement->bindValue(":mainPicture", $article->getMainPicture());
        $statement->bindValue(":content", $article->getContent());
        $statement->bindValue(":author", $article->getAuthor());
        $statement->bindValue(":date", $article->getDate()->format('Y-m-d'));
        $statement->bindValue(":views", $article->getViews(), PDO::PARAM_INT);
        $statement->execute();
        return new JsonResponse(null, 204);
    }

    
    public function delete(Article $article) {
        $statement = $this->connection->prepare('DELETE FROM article WHERE id=:id');
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);


        $statement->execute();

    }



}

