<?php

namespace App\Repository;

use App\Entities\Category;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $categorys = [];
        $statement = $this->connection->prepare('SELECT * FROM category');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $categorys[] = $this->sqlToCategory($item);
        }
        return $categorys;
    }

    public function findById(int $id):?Category {
        $statement = $this->connection->prepare('SELECT * FROM category WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToCategory($result);
        }
        return null;
    }

    private function sqlToCategory(array $line):Category {
        return new Category($line['name'], $line['id']);
    }

    public function persist(Category $category) {
        $statement = $this->connection->prepare('INSERT INTO category (name) VALUES (:name)');
        $statement->bindValue('name', $category->getName());
        $statement->execute();
        $category->setId($this->connection->lastInsertId());
    }


    public function update(Category $category)
    {
        $statement = $this->connection->prepare("UPDATE category SET name = :name WHERE id=:id");
        $statement->bindValue(":id", $category->getId(), PDO::PARAM_INT);
        $statement->bindValue(":name", $category->getName());
        $statement->execute();
        return new JsonResponse(null, 204);
    }

    public function deleteById(int $id):bool
    {
       
        $statement = $this->connection->prepare('DELETE FROM category WHERE id=:id');
        $statement->bindValue(':id', $id);

        $results= $statement->execute();
        
        return $results;

    }


}

