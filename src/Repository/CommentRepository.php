<?php

namespace App\Repository;


use App\Entities\Comment;
use DateTime;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        $comments = [];
        $statement = $this->connection->prepare('SELECT * FROM comment');
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $comments[] = $this->sqlToComment($item);
        }
        return $comments;
    }

    public function findById(int $id):?Comment {
        $statement = $this->connection->prepare('SELECT * FROM comment WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToComment($result);
        }
        return null;
    }

    private function sqlToComment(array $line):Comment {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Comment($line['author'], $date ,$line['content'], $line['id_article'], $line['id']);
    }

    public function persist(Comment $comment) {
        $statement = $this->connection->prepare('INSERT INTO comment (author,date,content, id_article) VALUES ( :author, :date,:content, :id_article)');
        $statement->bindValue('author', $comment->getAuthor());
        $statement->bindValue('date', $comment->getDate()->format('Y-m-d'));
        $statement->bindValue('content', $comment->getContent());
        $statement->bindValue('id_article', $comment->getId_article(), PDO::PARAM_INT);


        $statement->execute();

        $comment->setId($this->connection->lastInsertId());

    }


    public function update(Comment $comment)
    {
        $statement = $this->connection->prepare("UPDATE comment SET author=:author, date=:date,content=:content,id_article=:id_article WHERE id=:id");
        $statement->bindValue(":id", $comment->getId(), PDO::PARAM_INT);
        $statement->bindValue('author', $comment->getAuthor());
        $statement->bindValue('date', $comment->getDate()->format('Y-m-d'));
        $statement->bindValue('content', $comment->getContent());
        $statement->bindValue('id_article', $comment->getId_article(), PDO::PARAM_INT);
        $statement->execute();
        return new JsonResponse(null, 204);
    }

    public function deleteById(int $id):bool
    {
       
        $statement = $this->connection->prepare('DELETE FROM comment WHERE id=:id');
        $statement->bindValue(':id', $id);

        $results= $statement->execute();
        
        return $results;

    }


}

