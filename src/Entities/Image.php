<?php

namespace App\Entities;

class Image
{
    private ?int $id;
    private ?string $link;
    public ?int $id_article;

    /**
     * @param int|null $id
     * @param string|null $link
     * @param int|null $id_article
     */
    public function __construct( ?string $link, ?int $id_article,?int $id=null) {
    	$this->id = $id;
    	$this->link = $link;
    	$this->id_article = $id_article;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getLink(): ?string {
		return $this->link;
	}
	
	/**
	 * @param string|null $link 
	 * @return self
	 */
	public function setLink(?string $link): self {
		$this->link = $link;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getId_article(): ?int {
		return $this->id_article;
	}
	
	/**
	 * @param int|null $id_article 
	 * @return self
	 */
	public function setId_article(?int $id_article): self {
		$this->id_article = $id_article;
		return $this;
	}
}