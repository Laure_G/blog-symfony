<?php

namespace App\Entities;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Article
{
    private ?int $id;
    #[Assert\NotBlank]
	private ?string $title;
	#[Assert\NotBlank]
	private ?string $mainPicture;
	#[Assert\NotBlank]
    private ?string $content;
	#[Assert\NotBlank]
    private ?string $author;
	private ?DateTime $date;
    private ?int $views;
	

	/**
	 * @param int|null $id
	 * @param string|null $title
	 * @param string|null $mainPicture
	 * @param string|null $content
	 * @param string|null $author
	 * @param DateTime|null $date
	 * @param int|null $views
	 */
	public function __construct( ?string $title, ?string $mainPicture, ?string $content, ?string $author, ?DateTime $date, ?int $views,?int $id=null) {
		$this->id = $id;
		$this->title = $title;
		$this->mainPicture = $mainPicture;
		$this->content = $content;
		$this->author = $author;
		$this->date = $date;
		$this->views = $views;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getMainPicture(): ?string {
		return $this->mainPicture;
	}
	
	/**
	 * @param string|null $mainPicture 
	 * @return self
	 */
	public function setMainPicture(?string $mainPicture): self {
		$this->mainPicture = $mainPicture;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param string|null $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param string|null $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getViews(): ?int {
		return $this->views;
	}
	
	/**
	 * @param int|null $views 
	 * @return self
	 */
	public function setViews(?int $views): self {
		$this->views = $views;
		return $this;
	}
}