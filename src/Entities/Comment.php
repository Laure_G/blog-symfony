<?php

namespace App\Entities;
use DateTime;

class Comment
{
    private ?int $id;
    private ?string $author;
    private ?DateTime $date;
    private ?string $content;
    public ?int $id_article;

    /**
     * @param int|null $id
     * @param string|null $author
     * @param DateTime|null $date
     * @param string|null $content
     * @param int|null $id_article
     */
    public function __construct( ?string $author, ?DateTime $date, ?string $content, ?int $id_article, ?int $id=null) {
    	$this->id = $id;
    	$this->author = $author;
    	$this->date = $date;
    	$this->content = $content;
    	$this->id_article = $id_article;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param string|null $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param string|null $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	

	/**
	 * @return int|null
	 */
	public function getId_article(): ?int {
		return $this->id_article;
	}
	
	/**
	 * @param int|null $id_article 
	 * @return self
	 */
	public function setId_article(?int $id_article): self {
		$this->id_article = $id_article;
		return $this;
	}
}