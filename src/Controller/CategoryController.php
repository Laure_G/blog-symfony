<?php

namespace App\Controller;

use App\Entities\Category;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/category')]
class CategoryController extends AbstractController
{
    private CategoryRepository $repo;

    public function __construct(CategoryRepository $repo) {
    	$this->repo = $repo;
    }


    #[Route(methods: 'GET')]
    public function all() {
        
        $categorys = $this->repo->findAll();
        return $this->json($categorys);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $category = $this->repo->findById($id);
        if(!$category){
            throw new NotFoundHttpException();

        }
        return $this->json($category);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        
        $category = $serializer->deserialize($request->getContent(), Category::class, 'json');
        $this->repo->persist($category);

        return $this->json($category, Response::HTTP_CREATED);
    }

    #[Route( methods: 'PUT')]
    public function update(Request $request, SerializerInterface $serializer ){
        $category = $serializer->deserialize($request->getContent(), Category::class, 'json');
        $this->repo->update($category);

        return $this->json($category, Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function supp(int $id) {
       $category = $this->repo->deleteById($id);
       if(!$category) {
        throw new NotFoundHttpException();
        }
        return $this->json($category,204);
    }
}
