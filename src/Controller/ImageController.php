<?php

namespace App\Controller;

use App\Entities\Image;
use App\Repository\ImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/image')]
class ImageController extends AbstractController
{
    private ImageRepository $repo;

    public function __construct(ImageRepository $repo) {
    	$this->repo = $repo;
    }


    #[Route(methods: 'GET')]
    public function all() {
        
        $images = $this->repo->findAll();
        return $this->json($images);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $image = $this->repo->findById($id);
        if(!$image){
            throw new NotFoundHttpException();

        }
        return $this->json($image);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        
        $image = $serializer->deserialize($request->getContent(), Image::class, 'json');
        $this->repo->persist($image);

        return $this->json($image, Response::HTTP_CREATED);
    }

    #[Route( methods: 'PUT')]
    public function update(Request $request, SerializerInterface $serializer ){
        $image = $serializer->deserialize($request->getContent(), Image::class, 'json');
        $this->repo->update($image);

        return $this->json($image, Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function supp(int $id) {
       $image = $this->repo->deleteById($id);
       if(!$image) {
        throw new NotFoundHttpException();
        }
        return $this->json($image,204);
    }
}
